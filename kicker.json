{
  "name": "Python",
  "description": "Build and test [Python](https://www.python.org/) language",
  "template_path": "templates/gitlab-ci-python.yml",
  "kind": "build",
  "variables": [
    {
      "name": "PYTHON_IMAGE",
      "description": "The Docker image used to run Python - **it is highly recommended to set the specific version your project needs**",
      "default": "python:3"
    },
    {
      "name": "PYTHON_PROJECT_DIR",
      "description": "Python project root directory",
      "default": "."
    },
    {
      "name": "REQUIREMENTS_FILE",
      "description": "Full path to `requirements.txt` file _(relative to `$PYTHON_PROJECT_DIR`)_",
      "default": "requirements.txt",
      "advanced": true
    },
    {
      "name": "PYTHON_COMPILE_ARGS",
      "description": "[`compileall` CLI options](https://docs.python.org/3/library/compileall.html)",
      "default": "*",
      "advanced": true
    },
    {
      "name": "PIP_OPTS",
      "description": "pip extra [options](https://pip.pypa.io/en/stable/reference/pip/#general-options)",
      "advanced": true
    },
    {
      "name": "PYTHON_POETRY_DISABLED",
      "description": "Disable poetry support",
      "type": "boolean",
      "advanced": true
    },
    {
      "name": "PYTHON_POETRY_EXTRAS",
      "description": "Poetry [extra sets of dependencies](https://python-poetry.org/docs/pyproject/#extras) to include, space separated",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "pylint",
      "name": "pylint",
      "description": "Code analysis based on [pylint](http://pylint.pycqa.org/en/latest/) Python lib.",
      "enable_with": "PYLINT_ENABLED",
      "variables": [
        {
          "name": "PYLINT_ARGS",
          "description": "Additional [pylint CLI options](http://pylint.pycqa.org/en/latest/user_guide/run.html#command-line-options)",
          "advanced": true
        },
        {
          "name": "PYLINT_FILES",
          "description": "Files or directories to analyse",
          "advanced": true
        }
      ]
    },
    {
      "id": "unittest",
      "name": "unittest",
      "description": "Unit tests based on [unittest](https://docs.python.org/3/library/unittest.html) framework",
      "enable_with": "UNITTEST_ENABLED",
      "variables": [
        {
          "name": "TEST_REQUIREMENTS_FILE",
          "description": "Path to test requirements file _(relative to `$PYTHON_PROJECT_DIR`)_",
          "default": "test-requirements.txt",
          "advanced": true
        },
        {
          "name": "UNITTEST_ARGS",
          "description": "Additional xmlrunner/unittest CLI options",
          "advanced": true
        }
      ]
    },
    {
      "id": "pytest",
      "name": "pytest",
      "description": "Unit tests based on [pytest](https://docs.pytest.org/) framework",
      "enable_with": "PYTEST_ENABLED",
      "variables": [
        {
          "name": "TEST_REQUIREMENTS_FILE",
          "description": "Path to test requirements file _(relative to `$PYTHON_PROJECT_DIR`)_",
          "default": "test-requirements.txt",
          "advanced": true
        },
        {
          "name": "PYTEST_ARGS",
          "description": "Additional [pytest](https://docs.pytest.org/en/stable/usage.html) or [pytest-cov](https://github.com/pytest-dev/pytest-cov#usage) CLI options",
          "advanced": true
        }
      ]
    },
    {
      "id": "nosetest",
      "name": "nosetest",
      "description": "Unit tests based on [nose](https://nose.readthedocs.io/) framework",
      "enable_with": "NOSETESTS_ENABLED",
      "variables": [
        {
          "name": "TEST_REQUIREMENTS_FILE",
          "description": "Path to test requirements file _(relative to `$PYTHON_PROJECT_DIR`)_",
          "default": "test-requirements.txt",
          "advanced": true
        },
        {
          "name": "NOSETESTS_ARGS",
          "description": "Additional [nose CLI options](https://nose.readthedocs.io/en/latest/usage.html#options)",
          "advanced": true
        }
      ]
    },
    {
      "id": "bandit",
      "name": "Bandit",
      "description": "[Bandit](https://pypi.org/project/bandit/) analysis (SAST)",
      "enable_with": "BANDIT_ENABLED",
      "variables": [
        {
          "name": "BANDIT_ARGS",
          "description": "Additional [Bandit CLI options](https://github.com/PyCQA/bandit#usage)",
          "default": "--recursive .",
          "advanced": true
        }
      ]
    },
    {
      "id": "safety",
      "name": "Safety",
      "description": "Detect security vulnerabilities with [Safety](https://pypi.org/project/safety/) (dependencies analysis)",
      "enable_with": "SAFETY_ENABLED",
      "variables": [
        {
          "name": "SAFETY_ARGS",
          "description": "Additional [Safety CLI options](https://github.com/pyupio/safety#usage)",
          "default": "--full-report",
          "advanced": true
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish",
      "description": "Publish your code to a [Twine](https://pypi.org/project/twine/) repository",
      "variables": [
        {
          "name": "TWINE_REPOSITORY_URL",
          "type": "url",
          "description": "Twine repository url to publish you python project",
          "mandatory": true
        },
        {
          "name": "TWINE_USERNAME",
          "description": "Twine repository username credential",
          "secret": true,
          "mandatory": true
        },
        {
          "name": "TWINE_PASSWORD",
          "description": "Twine repository password credential",
          "secret": true,
          "mandatory": true
        }
      ]
    },
    {
      "id": "release",
      "name": "Release",
      "description": "Manually trigger a release of your code (uses [bumpversion](https://pypi.org/project/bumpversion/))",
      "variables": [
        {
          "name": "RELEASE_VERSION_PART",
          "type": "enum",
          "values": [
            "",
            "major",
            "minor",
            "patch"
          ],
          "description": "The part of the version to increase (one of: `major`, `minor`, `patch`)",
          "default": "minor",
          "advanced": true
        },
        {
          "name": "RELEASE_USERNAME",
          "description": "Username credential for Git push",
          "secret": true,
          "mandatory": true
        },
        {
          "name": "RELEASE_ACCESS_TOKEN",
          "description": "Password credential for Git push",
          "secret": true,
          "mandatory": true
        }
      ]
    },
    {
      "id": "docs",
      "name": "Docs",
      "description": "Documentation generation of your python code using [Sphinx](http://www.sphinx-doc.org/en/master/). Documentation will be available through a gitlab artifact.",
      "enable_with": "DOCS_ENABLED",
      "variables": [
        {
          "name": "DOCS_REQUIREMENTS_FILE",
          "description": "Python dependencies for documentation generation _(relative to `$PYTHON_PROJECT_DIR`)_",
          "default": "docs-requirements.txt",
          "advanced": true
        },
        {
          "name": "DOCS_DIRECTORY",
          "description": "Directory containing docs source",
          "default": "docs",
          "advanced": true
        },
        {
          "name": "DOCS_BUILD_DIR",
          "description": "Output build directory for documentation",
          "default": "public",
          "advanced": true
        },
        {
          "name": "DOCS_MAKE_ARGS",
          "description": "Args of make command",
          "default": "html BUILDDIR=${DOCS_BUILD_DIR}",
          "advanced": true
        }
      ]
    }
  ]
}
